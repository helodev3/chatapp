import React from 'react';
import { StyleSheet, Text, View,TouchableOpacity } from 'react-native';
import {Container,Content,Header,Thumbnail} from "native-base"

export default class Contact extends React.Component{
    constructor(props){
        super(props)
    }
    _minus=(nom)=>{
        return nom.toLowerCase()
  
    }
    render(){
      const urie = "https://facebook.github.io/react-native/docs/assets/favicon.png";
      
      
      
      return(
        <TouchableOpacity style={{width:"100%",height:50,flexDirection:"row",justifyContent:"flex-start",alignItems:"center"}}>
            <Thumbnail source={{uri:this.props.uri}} small circle style={{marginLeft:5}}/>
            <Text style={{marginHorizontal:15}}>{this.props.nom}</Text>
  
        </TouchableOpacity>)
    }
  }