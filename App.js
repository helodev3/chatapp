import React from 'react';
import { StyleSheet, Text, View ,TouchableOpacity,FlatList} from 'react-native';
import {Tab,Tabs,ScrollableTab ,Container,Content,Header,Thumbnail} from "native-base"
import Contact from "./components/contact"


export default class App extends React.Component {
  constructor(props){
    super(props)

    this.state={
      contactlist:[{nom:"Johnson Robert",uri:"http://www.mediafire.com/view/6dg46mubmt0syar/1.jpg/file"},
      {nom:"Eddie Page",uri:"http://www.mediafire.com/view/hfplzr9mnlixphz/2.jpg/file"},
      {nom:"Kevin Brewer",uri:"http://www.mediafire.com/file/e2gcix12av9grmx/3.jpg/file"},
      {nom:"Jackie Ronie",uri:"http://www.mediafire.com/file/lo3ko8o0p6aq3qo/4.jpg/file"},
      {nom:"Ben Quentin",uri:"http://www.mediafire.com/file/sioyaum5yrt549r/9.png/file"},
      {nom:"Dinesh Rajiski",uri:"http://www.mediafire.com/file/1pk69hf18a1s4ub/6.jpg/file"}
      ,{nom:"Magie Yuan",uri:"http://www.mediafire.com/file/0cjxivnppmkrijc/7.jpg/file"},
      {nom:"Ali Baba",uri:"http://www.mediafire.com/file/21soeyu6eoboeog/8.jpg/file"},
      {nom:"Maria Carlo",uri:"http://www.mediafire.com/file/v85v3c3vks7vyyd/5.jpg/file"}],
      messagelist:[{contact:"",lastmessage:"",time:""}]
    }
  }
  _rendercontact=(nom)=> (<Contact nom={nom} />  )

  render(){
    
    return (
    <Container >
      <Header hasTabs>

      </Header>
      <Tabs renderTabBar={()=><ScrollableTab/>}>
        <Tab heading="Messages">
          <View/>
        </Tab>
        <Tab heading="Contacts">
          <FlatList data={this.state.contactlist} 
          renderItem={({item})=>{return(<Contact nom={item.nom} uri={item.uri}/>)}}
          keyExtractor={(item,index)=>index.toString()}
          />
        
        </Tab>
        
      </Tabs>
    </Container>
  );}
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
